/*------------------PaDC-----------------------------
---------------Labwork #4----------------------------
------------Threads in OpenMP------------------------
--Group IO-52----------------------------------------
--Author: Butskiy Yuriy------------------------------
--Function 1 d = ((A*B) + (C*(B*(MA*MD))))-----------
--Function 2 MK = (TRANS(MA) * TRANS(MB * MM) + MX)--
--Function 3 T = (SORT(O + P) * TRANS(MR * MS)-------
--23.11.2017-----------------------------------------*/

#include "data.h"
#include <omp.h>
#include <Windows.h>

int N = 3;

int main()
{
	omp_set_num_threads(3);
#pragma omp parallel
	{
#pragma omp sections
		{
#pragma omp section
			{
				cout << "Task #1 started" << endl;
				Sleep(200);
				Vector A(N);
				Vector B(N);
				Vector C(N);
				Matrix MA(N, N);
				Matrix MD(N, N);
				Vector_Input(A);
				Vector_Input(B);
				Vector_Input(C);
				Matrix_Input(MA);
				Matrix_Input(MD);
				int d = F1(A, B, C, MA, MD);
				Sleep(1000);
				if (N < 10)
				{
					cout << "T1: d = " << d << endl;
				}

				cout << "Task #1 finished" << endl;
			}

#pragma omp section
			{
				cout << "Task #2 started" << endl;
				Sleep(200);
				Matrix MA(N, N);
				Matrix MB(N, N);
				Matrix MM(N, N);
				Matrix MX(N, N);
				Matrix_Input(MA);
				Matrix_Input(MB);
				Matrix_Input(MM);
				Matrix_Input(MX);
				Matrix MK = F2(MA, MB, MM, MX);
				Sleep(2000);
				if (N < 10)
				{
					cout << "T2: MK = " << endl;
					Matrix_Output(MK);
				}
				cout << "Task #2 finished" << endl;
			}
#pragma omp section
			{
				cout << "Task #3 started" << endl;
				Sleep(200);
				Vector O(N);
				Vector P(N);
				Matrix MR(N, N);
				Matrix MS(N, N);
				Vector_Input(O);
				Vector_Input(P);
				Matrix_Input(MR);
				Matrix_Input(MS);
				Vector T = F3(O, P, MR, MS);
				Sleep(3000);
				if (N < 10)
				{
					cout << "T3: T = " << endl;
					Vector_Output(T);
				}
				cout << "Task #3 finished" << endl;
			}
		}
	}

	cout << "Labwork finished" << endl;
	system("pause");
	return 0;
}