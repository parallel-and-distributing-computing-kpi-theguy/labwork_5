Laboratory work #5 for Parallel and Distributing Computing.

Purpose for the work: To study and practice how to create threads in programming libary - OpenMP.
Programming language: C++
Used technologies: OpenMP gives opportunity to create separate blocks of sequential program for one thread, with multithreading elements inside of it. To work with parallel section it's enough to write compiler pragma - #pragma omp parallel - that will parallelize specified area of code.

Controling of the program:
variable N stands for the size of Matrixes and Vectors respectively.
Program works for 3 threads, all returns the result.